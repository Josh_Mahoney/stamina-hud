// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STANIMA_HUD_Stanima_HUDHUD_generated_h
#error "Stanima_HUDHUD.generated.h already included, missing '#pragma once' in Stanima_HUDHUD.h"
#endif
#define STANIMA_HUD_Stanima_HUDHUD_generated_h

#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_RPC_WRAPPERS
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStanima_HUDHUD(); \
	friend struct Z_Construct_UClass_AStanima_HUDHUD_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), NO_API) \
	DECLARE_SERIALIZER(AStanima_HUDHUD)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStanima_HUDHUD(); \
	friend struct Z_Construct_UClass_AStanima_HUDHUD_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), NO_API) \
	DECLARE_SERIALIZER(AStanima_HUDHUD)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStanima_HUDHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStanima_HUDHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStanima_HUDHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStanima_HUDHUD(AStanima_HUDHUD&&); \
	NO_API AStanima_HUDHUD(const AStanima_HUDHUD&); \
public:


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStanima_HUDHUD(AStanima_HUDHUD&&); \
	NO_API AStanima_HUDHUD(const AStanima_HUDHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStanima_HUDHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStanima_HUDHUD)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_9_PROLOG
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_RPC_WRAPPERS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_INCLASS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_INCLASS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Stanima_HUD_Source_Stanima_HUD_Stanima_HUDHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
