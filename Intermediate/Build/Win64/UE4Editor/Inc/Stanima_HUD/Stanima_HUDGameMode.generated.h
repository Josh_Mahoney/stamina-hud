// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STANIMA_HUD_Stanima_HUDGameMode_generated_h
#error "Stanima_HUDGameMode.generated.h already included, missing '#pragma once' in Stanima_HUDGameMode.h"
#endif
#define STANIMA_HUD_Stanima_HUDGameMode_generated_h

#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_RPC_WRAPPERS
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStanima_HUDGameMode(); \
	friend struct Z_Construct_UClass_AStanima_HUDGameMode_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), STANIMA_HUD_API) \
	DECLARE_SERIALIZER(AStanima_HUDGameMode)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_INCLASS \
private: \
	static void StaticRegisterNativesAStanima_HUDGameMode(); \
	friend struct Z_Construct_UClass_AStanima_HUDGameMode_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), STANIMA_HUD_API) \
	DECLARE_SERIALIZER(AStanima_HUDGameMode)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STANIMA_HUD_API AStanima_HUDGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStanima_HUDGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STANIMA_HUD_API, AStanima_HUDGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STANIMA_HUD_API AStanima_HUDGameMode(AStanima_HUDGameMode&&); \
	STANIMA_HUD_API AStanima_HUDGameMode(const AStanima_HUDGameMode&); \
public:


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STANIMA_HUD_API AStanima_HUDGameMode(AStanima_HUDGameMode&&); \
	STANIMA_HUD_API AStanima_HUDGameMode(const AStanima_HUDGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STANIMA_HUD_API, AStanima_HUDGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStanima_HUDGameMode)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PlayerHUDClass() { return STRUCT_OFFSET(AStanima_HUDGameMode, PlayerHUDClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AStanima_HUDGameMode, CurrentWidget); }


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_8_PROLOG
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_RPC_WRAPPERS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_INCLASS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_INCLASS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Stanima_HUD_Source_Stanima_HUD_Stanima_HUDGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
