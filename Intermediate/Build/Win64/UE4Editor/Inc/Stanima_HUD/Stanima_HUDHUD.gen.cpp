// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Stanima_HUD/Stanima_HUDHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStanima_HUDHUD() {}
// Cross Module References
	STANIMA_HUD_API UClass* Z_Construct_UClass_AStanima_HUDHUD_NoRegister();
	STANIMA_HUD_API UClass* Z_Construct_UClass_AStanima_HUDHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Stanima_HUD();
// End Cross Module References
	void AStanima_HUDHUD::StaticRegisterNativesAStanima_HUDHUD()
	{
	}
	UClass* Z_Construct_UClass_AStanima_HUDHUD_NoRegister()
	{
		return AStanima_HUDHUD::StaticClass();
	}
	struct Z_Construct_UClass_AStanima_HUDHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStanima_HUDHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Stanima_HUD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStanima_HUDHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "Stanima_HUDHUD.h" },
		{ "ModuleRelativePath", "Stanima_HUDHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStanima_HUDHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStanima_HUDHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStanima_HUDHUD_Statics::ClassParams = {
		&AStanima_HUDHUD::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008002ACu,
		nullptr, 0,
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AStanima_HUDHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AStanima_HUDHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStanima_HUDHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStanima_HUDHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStanima_HUDHUD, 3437953782);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStanima_HUDHUD(Z_Construct_UClass_AStanima_HUDHUD, &AStanima_HUDHUD::StaticClass, TEXT("/Script/Stanima_HUD"), TEXT("AStanima_HUDHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStanima_HUDHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
