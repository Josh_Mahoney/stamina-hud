// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef STANIMA_HUD_Stanima_HUDProjectile_generated_h
#error "Stanima_HUDProjectile.generated.h already included, missing '#pragma once' in Stanima_HUDProjectile.h"
#endif
#define STANIMA_HUD_Stanima_HUDProjectile_generated_h

#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStanima_HUDProjectile(); \
	friend struct Z_Construct_UClass_AStanima_HUDProjectile_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), NO_API) \
	DECLARE_SERIALIZER(AStanima_HUDProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStanima_HUDProjectile(); \
	friend struct Z_Construct_UClass_AStanima_HUDProjectile_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), NO_API) \
	DECLARE_SERIALIZER(AStanima_HUDProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStanima_HUDProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStanima_HUDProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStanima_HUDProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStanima_HUDProjectile(AStanima_HUDProjectile&&); \
	NO_API AStanima_HUDProjectile(const AStanima_HUDProjectile&); \
public:


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStanima_HUDProjectile(AStanima_HUDProjectile&&); \
	NO_API AStanima_HUDProjectile(const AStanima_HUDProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStanima_HUDProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStanima_HUDProjectile)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AStanima_HUDProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AStanima_HUDProjectile, ProjectileMovement); }


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_9_PROLOG
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_RPC_WRAPPERS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_INCLASS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Stanima_HUD_Source_Stanima_HUD_Stanima_HUDProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
