// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STANIMA_HUD_Stanima_HUDCharacter_generated_h
#error "Stanima_HUDCharacter.generated.h already included, missing '#pragma once' in Stanima_HUDCharacter.h"
#endif
#define STANIMA_HUD_Stanima_HUDCharacter_generated_h

#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_RPC_WRAPPERS \
	virtual bool MyServerFunction_Validate(); \
	virtual void MyServerFunction_Implementation(); \
 \
	DECLARE_FUNCTION(execUpdateCurrentStamina) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Stamina); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCurrentStamina(Z_Param_Stamina); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMyServerFunction) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->MyServerFunction_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("MyServerFunction_Validate")); \
			return; \
		} \
		P_THIS->MyServerFunction_Implementation(); \
		P_NATIVE_END; \
	}


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void MyServerFunction_Implementation(); \
 \
	DECLARE_FUNCTION(execUpdateCurrentStamina) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Stamina); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCurrentStamina(Z_Param_Stamina); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMyServerFunction) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->MyServerFunction_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("MyServerFunction_Validate")); \
			return; \
		} \
		P_THIS->MyServerFunction_Implementation(); \
		P_NATIVE_END; \
	}


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_EVENT_PARMS
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_CALLBACK_WRAPPERS
#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStanima_HUDCharacter(); \
	friend struct Z_Construct_UClass_AStanima_HUDCharacter_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), NO_API) \
	DECLARE_SERIALIZER(AStanima_HUDCharacter)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAStanima_HUDCharacter(); \
	friend struct Z_Construct_UClass_AStanima_HUDCharacter_Statics; \
public: \
	DECLARE_CLASS(AStanima_HUDCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Stanima_HUD"), NO_API) \
	DECLARE_SERIALIZER(AStanima_HUDCharacter)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStanima_HUDCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStanima_HUDCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStanima_HUDCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStanima_HUDCharacter(AStanima_HUDCharacter&&); \
	NO_API AStanima_HUDCharacter(const AStanima_HUDCharacter&); \
public:


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStanima_HUDCharacter(AStanima_HUDCharacter&&); \
	NO_API AStanima_HUDCharacter(const AStanima_HUDCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStanima_HUDCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStanima_HUDCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStanima_HUDCharacter)


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AStanima_HUDCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AStanima_HUDCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AStanima_HUDCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AStanima_HUDCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AStanima_HUDCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AStanima_HUDCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AStanima_HUDCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AStanima_HUDCharacter, L_MotionController); } \
	FORCEINLINE static uint32 __PPO__InitialStamina() { return STRUCT_OFFSET(AStanima_HUDCharacter, InitialStamina); } \
	FORCEINLINE static uint32 __PPO__CurrentStamina() { return STRUCT_OFFSET(AStanima_HUDCharacter, CurrentStamina); }


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_11_PROLOG \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_EVENT_PARMS


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_RPC_WRAPPERS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_CALLBACK_WRAPPERS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_INCLASS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_CALLBACK_WRAPPERS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Stanima_HUD_Source_Stanima_HUD_Stanima_HUDCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
