// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Stanima_HUD.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Stanima_HUD, "Stanima_HUD" );
 