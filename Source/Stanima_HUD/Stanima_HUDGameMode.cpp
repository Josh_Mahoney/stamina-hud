// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Stanima_HUDGameMode.h"
#include "Stanima_HUD.h"
#include "Stanima_HUDHUD.h"
#include "Stanima_HUDCharacter.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


void AStanima_HUDGameMode::BeginPlay()
{
	Super::BeginPlay();

	AStanima_HUDCharacter* MyCharacter = Cast<AStanima_HUDCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (PlayerHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);

		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

AStanima_HUDGameMode::AStanima_HUDGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AStanima_HUDHUD::StaticClass();
}
