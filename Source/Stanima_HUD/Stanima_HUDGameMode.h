// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Stanima_HUDGameMode.generated.h"

UCLASS(minimalapi)
class AStanima_HUDGameMode : public AGameModeBase
{
	GENERATED_BODY()

	

		//Overrides on game start
		virtual void BeginPlay() override;

public:
	AStanima_HUDGameMode();


protected:
	
	//Allows the blueprints to be seen in the editor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina", Meta = (BlueprintProtected = "true"))

	TSubclassOf<class UUserWidget> PlayerHUDClass;

	UPROPERTY()
	class UUserWidget* CurrentWidget;

};


